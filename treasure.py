restart = 1
f = 0
while restart < 2:
    line1 = ["⬜️", "⬜️", "⬜️"]
    line2 = ["⬜️", "⬜️", "⬜️"]
    line3 = ["⬜️", "⬜️", "⬜️"]
    treasure_map = [line1, line2, line3]
    
    print("Hiding your treasure! 'X' marks the spot.")
    position = input("Ahoy, matey! Ready to stash your loot? Remember, it's all about the ABC's and 123's! So pick your spot with a sprinkle of alphabet magic and a dash of numerical charm. Let the treasure hunt begin! Arrr!\nLike: A1\tYour hiding place: ").lower()
    
    if position[0] == 'a':
        f = 0
    elif position[0] == 'b':
        f = 1
    elif position[0] == 'c':
        f = 2
    
    try:
        e = int(position[1]) - 1
        treasure_map[e][f] = 'X'
        print(f"{line1}\n{line2}\n{line3}")
        break
        
    except (IndexError, ValueError):
        print("Oops! Looks like you've wandered off the treasure map! Remember, stick to A, B, C or 1, 2, 3 for hiding your loot!")
        
        while True:
            choice = input("Hit that 'R' key to jump back into the game and hide your fortune away, or type 'Q' to gracefully bow out and quit. Let the adventure continue! ").lower()
            
            if choice == 'q':
                restart = 2
                break
            elif choice == 'r':
                restart = 1
                break